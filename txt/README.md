# TXT (archivo de entrada para `create_jsons.rb`)

Los documentos de texto operan con estos supuestos:

* Cada línea será una línea nueva del juego.
* En el modo historia es posible dejar líneas vacías para señalar 
  nuevos párrafos.
* En el modo arcade las palabras siempre se mostrarán en orden 
  aleatorio.
* Si se quiere un JSON para una historia, el nombre del archivo tiene
  que contener `story`.
* Si se quiere un JSON para arcade, el nombre del archivo tiene que 
  contener `arcade`.
