# Foo library

Librería para desarrollar juegos tipo Foo.

=> [DEMO](https://programando-libreros.gitlab.io/ludico/foo-library/).

## Reglas

### Objetivo

1. Completa las palabras o frases de cada línea hasta que no haya más.

### Elementos del juego

1. Cada ficha contiene diferentes letras y número de oportunidades.
2. Cada línea contiene diferentes fichas.

### Cómo jugar

1. Presiona la ficha para seleccionar la letra pertinente.
    * ¡Cuidado!, solo tienes `n` oportunidades para dar con la letra correcta.
    * ¡Cuidado!, entre las opciones pueden aparecer bombas o bonos que afectan tu puntaje.
4. Si todas las letras de una línea son correctas o ya no hay oportunidades o ambas, se abre la siguiente línea hasta terminar el juego.

### Cómo ganar

1. Completa todas las líneas.

### Cómo perder

1. Si el tiempo es menor a 0. 
    * En el modo historia el tiempo es progresivo, pero en el modo arcade es regresivo.
2. Si el puntaje es menor a 0.

## Uso

Solo añade el script al documento HTML:

```
<script type="text/javascript" src="ruta/a/foo-library.min.js"></script>
```

## Opciones

Véase el README del directorio `lib`.

## Árbol de directorios

* `js`. _Script_ para el ejemplo.
* `json`. Archivos de salida necesarios para esta librería.
* `lib`. _Script_ de esta librería.
* `rb`. _Script_ para convertir los TXT a JSON.
* `txt`. Archivos de entrada para generar los JSON necesarios para esta librería.

## Listas de palabras

Para las palabras en español se usó el [_Lemario del DRAE_](http://olea.org/proyectos/lemarios/), RAE.

Para las palabras en inglés se usó [_An English Word List_](http://www-personal.umich.edu/~jlawler/wordlist.html), University of Michigan.

## Licencia

Esta librería está bajo [MIT License](https://choosealicense.com/licenses/mit/).
